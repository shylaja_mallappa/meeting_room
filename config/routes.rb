Rails.application.routes.draw do
  
  resources :events
get 'static_pages/home'
get  '/help',    to: 'static_pages#help'
get  '/about',   to: 'static_pages#about'
get  '/contact', to: 'static_pages#contact'
get  '/signup',  to: 'users#new'

  
post '/signup', to: 'users#create'
get  '/login',   to: 'sessions#new'
post 'bookings' , to:'bookings#create'
#get '/open_newpage_forbook' ,to: 'userbooking#new'
#get 'new_booking' , to:'bookings#index'
#get 'book_new_room' , to:'userbooking#create'
#get '/bookings', to:'bookings#create'
#get '/bookings_new', to: 'bookings#new'
#post   'room_book' , to: 'bookings#create'


post   '/login',   to: 'sessions#create'
delete '/logout',  to: 'sessions#destroy'

resources :users
resources :account_activations, only: [:edit]
resources :user_bookings
resources :rooms do 
	resources :bookings
end
resources :demos
resources :user_apps
resources :microposts
resources :tests
  

root 'static_pages#home'

end
