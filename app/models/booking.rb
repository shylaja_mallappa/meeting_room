class Booking < ApplicationRecord
	belongs_to :user
	belongs_to :room

	validate :check_date_uniqueness
	validates_presence_of :start_at
	validates_presence_of :end_at


	def check_date_uniqueness
	    
		if Booking.where("start_at = ? AND end_at = ? AND room_id = ?",start_at,end_at,room_id).exists?
			errors.add("room is not available on this date")
			# return false
		else
			return true
		end
	end
	

	
end


