class Room < ApplicationRecord
	has_many :room_facilities 
	has_many :facilities, through: :room_facilities
	
	has_many :bookings
	has_many :users,through: :bookings

end
