class BookingsController < ApplicationController
     before_action :get_booking, only: [:show, :update, :destroy, :edit]
     before_action :get_room
     before_action :get_booked_room
     

    def show
    end

    def index
    end

	def new
        @booking = Booking.new
    end

    def create
       
        @booking = @room.bookings.new(booking_params.merge(user_id: current_user.id))
        
        respond_to do |format|
          if @booking.save
            format.html { redirect_to room_booking_path(@room, @booking), notice: 'Your booking is confirmed.' }
            format.json { render :show, status: :created, location: @booking }
          else
            format.html { render :new }
            format.json { render json: @booking.errors, status: :unprocessable_entity }
          end
        end
    end
    

private

def get_booking
    @booking = Booking.find(params[:id])    
end

def get_room
    @room = Room.find(params[:room_id])
end


def booking_params
    params.require(:booking).permit(:start_at, :end_at, :room_id)
end

def get_booked_room
     @booked_rooms= @room.bookings.where("start_at > ? AND end_at < ?",Time.now,Time.now + 7.days )
     @booked_room = @booked_rooms.map{|a| {title: a.user.name, start: a.start_at, end: a.end_at}.as_json}

end


end
