class CreateRooms < ActiveRecord::Migration[5.0]
  #its a lookup table, and hold al rooms related info
  def change
    create_table :rooms do |t|
      
      t.string :room_name
      t.integer :capacity
      t.boolean :is_availiable

      t.timestamps
    end
  end
end
