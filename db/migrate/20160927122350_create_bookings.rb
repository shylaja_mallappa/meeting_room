class CreateBookings < ActiveRecord::Migration[5.0]
  def change
    create_table :bookings do |t|
      
      t.integer :user_id
      t.datetime :start_at
      t.datetime :end_at
      t.timestamps
    end
  end
end
