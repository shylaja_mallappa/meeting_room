class CreateFacilities < ActiveRecord::Migration[5.0]
  def change
    create_table :facilities do |t|
      t.string :title
      t.string :description
      t.boolean :is_availiable

      t.timestamps
    end
  end
end
