# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar")

40.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  
  
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               room_id: [1, 2, 3, 4, 5].sample)
end

r1 = Room.create({room_name: "meeting room 1", capacity: 5, is_availiable: true})
r2 = Room.create({room_name: "lohgadh", capacity: 12, is_availiable: true})
r3 = Room.create({room_name: "pratapgardh", capacity: 8, is_availiable: true})
r4 = Room.create({room_name: "war room", capacity: 15, is_availiable: true})
r5 = Room.create({room_name: "board room", capacity: 20, is_availiable: true})

f1 = Facility.create({title: "phone", is_availiable: true})
f2 = Facility.create({title: "voip",  is_availiable: true})
f3 = Facility.create({title: "projector", is_availiable: true})
f4 = Facility.create({title: "white board",  is_availiable: true})
f5 = Facility.create({title: "LED TV",  is_availiable: true})

[r1, r2, r3, r4, r5].each do |room|
  (1..5).to_a.sample.times do |index|
    RoomFacility.create(room_id: [1, 2, 3, 4, 5].sample, facility_id: [f1,f2,f3,f4,f5].sample.id)
  end
end


User.all.each_with_index do |u, index|
  start_at = Time.parse("#{(30..31).to_a.sample}-12-2016 10:00:00") + ((1..8).to_a.sample).hours
  end_at = start_at + 2.hours
  Booking.create!({user_id: u.id, room_id: [1, 2, 3, 4, 5].sample, start_at: start_at, end_at: end_at})
end